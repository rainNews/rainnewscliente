import { browser, by, element, ElementFinder } from 'protractor';

export class LoginPage {
    navigateToLogin(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'login') as Promise<unknown>;
    }

    navigateToNoticias(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'noticias') as Promise<unknown>;
    }

    navigateToTemas(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'temas') as Promise<unknown>;
    }

    navigateToMisHistorias(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'misHistorias') as Promise<unknown>;
    }

    navigateToTimeline(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'timeLine') as Promise<unknown>;
    }

    navigateToTendencias(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'tendencia') as Promise<unknown>;
    }

    navigateToFavoritos(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'favoritos') as Promise<unknown>;
    }

    navigateToPerfil(): Promise<unknown> {
        return browser.get(browser.baseUrl + 'perfil') as Promise<unknown>;
    }

    getEmailTextBox(): ElementFinder {
        return element(by.name('correo'));
    }

    getPasswordTextBox(): ElementFinder {
        return element(by.name('password'));
    }

    getSubmitButton(): ElementFinder {
        return element(by.id('btnLogin'));
    }

    getNoticiasTitleText(): Promise<string> {
        return element(by.id('portfolio')).element(by.css('h2')).getText() as Promise<string>;
    }

    getTemaTitleText(): Promise<string> {
        return element(by.id('divTemas')).element(by.css('h2')).getText() as Promise<string>;
    }

    getMisHistoriasTitleText(): Promise<string> {
        return element(by.id('divMisHistorias')).element(by.css('h2')).getText() as Promise<string>;
    }

    getTimelineTitleText(): Promise<string> {
        return element(by.id('divTimeline')).element(by.css('h2')).getText() as Promise<string>;
    }

    getTendenciasTitleText(): Promise<string> {
        return element(by.id('divTendencias')).element(by.css('h2')).getText() as Promise<string>;
    }

    getFavoritosTitleText(): Promise<string> {
        return element(by.id('divFavoritos')).element(by.css('h2')).getText() as Promise<string>;
    }

    getPerfilTitleText(): Promise<string> {
        return element(by.id('divPerfil')).element(by.css('h2')).getText() as Promise<string>;
    }
}
