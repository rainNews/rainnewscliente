import { LoginPage } from './login.po';
import { browser, logging, protractor } from 'protractor';

describe('Login tests', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
  });

  it('Should set user id value to session storage', async () => {
    page.navigateToLogin();
    page.getEmailTextBox().sendKeys('prueba0@gmail.com');
    page.getPasswordTextBox().sendKeys('prueba0');
    page.getSubmitButton().click();
    await browser.sleep(5000);
    const valLocalStorage = browser.executeScript('return window.sessionStorage.getItem(\'userid\');');
    expect(valLocalStorage).toEqual('1');
  });

  it('Should show noticias', async () => {
    page.navigateToNoticias();
    expect(page.getNoticiasTitleText()).toEqual('NOTICIAS');
  });

  it('Should show temas', async () => {
    page.navigateToTemas();
    expect(page.getTemaTitleText()).toEqual('TEMA DISPONIBLES');
  });

  it('Should show mis historias', async () => {
    page.navigateToMisHistorias();
    expect(page.getMisHistoriasTitleText()).toEqual('NUEVAS NOTICIAS');
  });

  it('Should show timeline', async () => {
    page.navigateToTimeline();
    expect(page.getTimelineTitleText()).toEqual('TIME LINE');
  });

  it('Should show tendencias', async () => {
    page.navigateToTendencias();
    expect(page.getTendenciasTitleText()).toEqual('TENDENCIA');
  });

  it('Should show favoritos', async () => {
    page.navigateToFavoritos();
    expect(page.getFavoritosTitleText()).toEqual('FAVORITOS');
  });

  it('Should show Perfil', async () => {
    page.navigateToPerfil();
    expect(page.getPerfilTitleText()).toEqual('PERFIL');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
