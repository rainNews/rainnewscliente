import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro-component',
  templateUrl: './registro-component.component.html',
  styleUrls: ['./registro-component.component.css']
})
export class RegistroComponentComponent implements OnInit {

  correo: string = '';
  nombre: string = '';
  apellido: string = '';
  password: string = '';

  alerta: string = '';

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
  }

  createRegisterObject (correo: string, nombre: string, apellido: string, password: string) {
    return {correo: correo, nombre: nombre, apellido: apellido, password: password};
  }

  register() {
    let registro = this.createRegisterObject(this.correo,this.nombre,this.apellido,this.password);
    this.con.PostRequest('registroUsuario', registro).toPromise()
    .then((res)=>{
      this.router.navigate(['login']);
    })
    .catch((err)=>{
      console.log(err.error[0].constraints);
      setTimeout(() => this.alerta = JSON.stringify(err.error[0].constraints), 0);
    });
  }
}
