import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-comentario',
  templateUrl: './editar-comentario.component.html',
  styleUrls: ['./editar-comentario.component.css']
})
export class EditarComentarioComponent implements OnInit {
  userid: string;
  comentarioid:string;
  comentario: any = [];
  comen:any;
  nuevoEditadorComentario: string;
  exito: string;
  alerta: string = '';
  color:string = "#65ff00";
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    this.comentarioid = sessionStorage.getItem('comentarioid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    else if(!this.comentarioid){
      this.router.navigate(['comentarios']);
    }
    this.mostrar();
  }

  createEditarComentarioObject(comentarioid:string, comentario: string) {
    return { comentarioid: comentarioid, comentario:comentario };
  }
  
  createLeerComentarioObject(comentarioid:string) {
    return { comentarioid: comentarioid };
  }
  
  mostrar(): void {
    console.log("este es el id del comentario"+this.comentarioid);
    let comentario = this.createLeerComentarioObject(this.comentarioid);
    this.con.PostRequest('getComentario',comentario).toPromise()
    .then((res)=>{
        for(let v in res){
          this.comentario = res[v];
          this.nuevoEditadorComentario= this.comentario.comentario;
          console.log(this.comentario);
        }
    }).catch((err) => {
      setTimeout(()=> this.alerta = err.console.error.mensaje,0);
    });
  }

  Editar():void{
    console.log("este es el id del comentario"+this.comentarioid);
    let comentario = this.createEditarComentarioObject(this.comentarioid, this.nuevoEditadorComentario);
    this.con.PostRequest('postUpdateComentario',comentario).toPromise()
    .then(()=>{
      this.color = "#65ff00";
      this.exito = "Cambio realizado !!!"
      setTimeout(() => {
        this.router.navigate(['comentarios']);
       }, 2000);
    }).catch((err)=>{
      setTimeout(()=> this.alerta = err.console.error.mensaje, 0);
    });
  }

}
