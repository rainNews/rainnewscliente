import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-temas',
  templateUrl: './temas.component.html',
  styleUrls: ['./temas.component.css']
})
export class TemasComponent implements OnInit {
  alerta: string = '';
  valores: any = [];
  temas: any = [];
  userid: string;

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createTemaUsuarioObject(userid: string, temaid: string) {
    return { userid: userid, temaid: temaid };
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }

  agregar(event) {
    let temaUsuario = this.createTemaUsuarioObject(this.userid, event.target.attributes.id.nodeValue);
    this.con.PostRequest('temaUsuario', temaUsuario).toPromise()
      .then((res) => {
        this.mostrar();
      })
      .catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  eliminar(event) {
    let temaUsuario = this.createTemaUsuarioObject(this.userid, event.target.attributes.id.nodeValue);
    this.con.PostRequest('deleteTemaUsuario', temaUsuario).toPromise()
      .then(() => {
        this.mostrar();
      })
      .catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  mostrar(): void {
    this.con.GetRequest('temas').toPromise()
      .then((res) => {
        for (let i in res) {
          this.valores = res[i];
        }
      })
      .catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });

    let usuario = this.createUsuarioObject(this.userid)
    this.con.PostRequest('temasUsuario', usuario).toPromise()
      .then((res) => {
        for (let i in res) {
          this.temas = res[i];
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

}
