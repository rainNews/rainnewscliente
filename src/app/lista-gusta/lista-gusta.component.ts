import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConectionService } from '../conection.service';

@Component({
  selector: 'app-lista-gusta',
  templateUrl: './lista-gusta.component.html',
  styleUrls: ['./lista-gusta.component.css']
})

export class ListaGustaComponent implements OnInit {
  alerta: string = '';
  noticias: any = [];
  userid: string;
  imagen: string = "assets/img/heart.png";

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }
  
  createNoticiaObject(userid: string,noticiaid: string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  createUsuarioComentarioObject(userid: string, noticiaid:string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  mostrar(): void {
    let noticia = sessionStorage.getItem("noticiaid");
    console.log(noticia);
    let not = this.createNoticiaObject(this.userid, noticia);
    this.con.PostRequest('postNoticiaUsuariosLikes', not).toPromise()
      .then((res) => {
        console.log(res);
        for (let i in res) {
          this.noticias = res[i];
          console.log(this.noticias);
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  like(event){
    alert("change picture");
    this.imagen  = "assets/img/heart2.png";
    console.log("este tendria que ser el id"+event.target.attributes.id.nodeValue);
    sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    console.log(event.target.attributes.id.nodeValue);
  }  

  listaGustar(event){
    alert("holi");
    //sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    this.router.navigate(['listagusta']);
  }

  submit(event){
    console.log("este tendria que ser el id"+event.target.attributes.id.nodeValue);
    sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    this.router.navigate(['comentarios']);
    console.log(event.target.attributes.id.nodeValue);
  }
}
