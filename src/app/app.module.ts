import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ConectionService } from './conection.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistroComponentComponent } from './registro-component/registro-component.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { PerfilComponent } from './perfil/perfil.component';
import { InicioComponent } from './inicio/inicio.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { MyMaterialModule } from './material.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TemasComponent } from './temas/temas.component';
import { TimelineComponent } from './timeline/timeline.component';
import { MisHistoriasComponent } from './mis-historias/mis-historias.component';
import { ComentariosComponent } from './comentarios/comentarios.component';
import { MenuComponent } from './menu/menu.component';
import { FavoritosComponent } from './favoritos/favoritos.component';
import { ListaGustaComponent } from './lista-gusta/lista-gusta.component';
import { EditarComentarioComponent } from './editar-comentario/editar-comentario.component';
import { EditarNoticiaComponent } from './editar-noticia/editar-noticia.component';
import { TendenciasComponent } from './tendencias/tendencias.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponentComponent,
    LoginComponentComponent,
    NoticiasComponent,
    PerfilComponent,
    InicioComponent,
    CategoriasComponent,
    TemasComponent,
    TimelineComponent,
    MisHistoriasComponent,
    ComentariosComponent,
    MenuComponent,
    FavoritosComponent,
    ListaGustaComponent,
    EditarComentarioComponent,
    EditarNoticiaComponent,
    TendenciasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'register', component: RegistroComponentComponent },
      { path: 'login', component: LoginComponentComponent },
      { path: 'inicio', component: InicioComponent },
      { path: 'categorias', component: CategoriasComponent },
      { path: 'noticias', component: NoticiasComponent },
      { path: 'perfil', component: PerfilComponent },
      { path: 'temas', component: TemasComponent },
      { path: 'misHistorias', component: MisHistoriasComponent },
      { path: 'timeLine', component: TimelineComponent },
      { path: 'comentarios', component: ComentariosComponent },
      { path: 'listagusta', component: ListaGustaComponent },
      { path: 'favoritos', component: FavoritosComponent },
      { path: 'editarComentario', component: EditarComentarioComponent },
      { path: 'editarNoticia', component: EditarNoticiaComponent },
      { path: 'tendencia', component: TendenciasComponent },
      { path: '**', redirectTo: 'inicio', pathMatch: 'full' }
    ])
  ],
  providers: [ConectionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
