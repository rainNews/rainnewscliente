import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.css']
})

export class ComentariosComponent implements OnInit {

  alerta: string = '';
  temas: any = [];
  comentarios: any = [];
  comen: any = [];
  noticias: any = [];
  userid: string;
  noticiaid:string;
  nuevoComentario: string;
  imagen: string = "assets/img/editar.png";
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    this.noticiaid = sessionStorage.getItem('noticiaid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    else if(!this.noticiaid){
      this.router.navigate(['misHistorias']);
    }
    this.mostrar();
    this.mostrarTimeLine();
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }
  
  createComentarioObject(noticiaid:string) {
    return { noticiaid: noticiaid };
  }

  
  createNuevoComentarioObject(noticiaid:string, userid:string, comentario: string){
    return { noticiaid: noticiaid, userid: userid, comentario: comentario };
  }

  mostrarTimeLine(): void {
    let usuario = this.createUsuarioObject(this.userid)
    this.con.PostRequest('postTimeLineUsuario', usuario).toPromise()
      .then((res) => {
        for (let i in res) {
          this.noticias = res[i];
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  mostrar(): void {
    let comentario = this.createComentarioObject(this.noticiaid);
    this.con.PostRequest('postComentarios',comentario).toPromise()
    .then((res)=>{
      for(let v in res){
        this.comentarios = res[v];
        this.comen = this.comentarios.comentarios;
        console.log(this.comentarios);
        console.log(this.comen);
      }
    }).catch((err) => {
      setTimeout(()=> this.alerta = err.console.error.mensaje,0);
    });
  }

  publicar(event) {
    let comentario = this.createNuevoComentarioObject(this.noticiaid, this.userid, this.nuevoComentario);
    this.con.PostRequest('postComentario',comentario).toPromise()
    .then(()=>{
      this.mostrar();
      this.nuevoComentario = "";
    }).catch((err)=>{
      setTimeout(()=> this.alerta = err.console.error.mensaje, 0);
    });
  }

  eliminar(event) {
    /*
    let noticia = this.createUsuarioNoticiaObject(this.userid, event.target.attributes.id.nodeValue);
    this.con.PostRequest('deleteNoticiaUsuario', noticia).toPromise()
      .then(() => {
        this.mostrar();
      })
      .catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
      */
  }

  editarComentario(event) {
    console.log("este tendria que ser el idComentario" + event.target.attributes.id.nodeValue);
    sessionStorage.setItem('comentarioid', event.target.attributes.id.nodeValue);
    this.router.navigate(['editarComentario']);
    console.log(event.target.attributes.id.nodeValue);
  }

}
