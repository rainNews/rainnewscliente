import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})

export class TimelineComponent implements OnInit {

  alerta: string = '';
  noticias: any = [];
  userid: string;
  imagen: string = "assets/img/heart0.png";
  editar: string = "assets/img/editar.png";

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }

  createNoticiaObject(userid: string, noticiaid: string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  createUsuarioComentarioObject(userid: string, noticiaid: string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  mostrar(): void {
    let usuario = this.createUsuarioObject(this.userid)
    this.con.PostRequest('postTimeLineUsuario', usuario).toPromise()
      .then((res) => {
        this.noticias = res.noticias;
        for (let i in this.noticias) {
          this.noticias[i].imagen = "assets/img/heart0.png";
          for (let j in res.usuario.noticiasLike) {
            if (res.usuario.noticiasLike[j].noticiaid === this.noticias[i].noticiaid) {
              this.noticias[i].imagen = "assets/img/heart2.png";
            }
          }
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  like(event) {
    sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    let noticia = sessionStorage.getItem('noticiaid');
    let not = this.createNoticiaObject(this.userid, noticia);
    this.con.PostRequest('postNoticiaLike', not).toPromise()
      .then((res) => {
        this.mostrar();
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  listaGustar(event) {
    sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    this.router.navigate(['listagusta']);
  }

  submit(event) {
    console.log("este tendria que ser el id" + event.target.attributes.id.nodeValue);
    sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    this.router.navigate(['comentarios']);
    console.log(event.target.attributes.id.nodeValue);
  }
}
