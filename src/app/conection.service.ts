import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers : new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const httpAddress = 'https://rainnewsbackend.herokuapp.com/';

@Injectable({
  providedIn: 'root'
})
export class ConectionService {

  constructor(private httpClient: HttpClient) { }

  PostRequest(serverAddress: string, info: object) {
    return this.httpClient.post<any>(httpAddress + serverAddress, info, httpOptions);
  }

  GetRequest(serverAddress: string) {
    return this.httpClient.get<any>(httpAddress + serverAddress, httpOptions);
  }
}
