import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';
import { not } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-mis-historias',
  templateUrl: './mis-historias.component.html',
  styleUrls: ['./mis-historias.component.css']
})

export class MisHistoriasComponent implements OnInit {

  alerta: string = '';
  temas: any = [];
  noticias: any = [];
  userid: string;
  noticia:string;
  editar: string = "assets/img/editar.png";

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }

  createUsuarioComentarioObject(userid: string, noticiaid:string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  createUsuarioNoticiaObject(userid: string, noticiaid: string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  createUsuarioTemaObject(userid: string, temaid: string, noticia: string) {
    return { userid: userid, temaid: temaid, noticia: noticia };
  }

  mostrar(): void {
    let usuario = this.createUsuarioObject(this.userid)
    this.con.PostRequest('temasUsuario', usuario).toPromise()
      .then((res) => {
        for (let i in res) {
          this.temas = res[i];
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });

      this.con.PostRequest('noticiasUsuario', usuario).toPromise()
      .then((res)=>{
        for(let i in res) {
          this.noticias=res[i]
        }
      }).catch((err)=> {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      })
  }

  publicar(event) {
    let noticia = this.createUsuarioTemaObject(this.userid, event.target.attributes.id.nodeValue, this.noticia);
    console.log(this.noticia);
    this.con.PostRequest('postNoticiaUsuario', noticia).toPromise()
    .then(() => {
      this.mostrar();
      this.noticia='';
    })
    .catch((err) => {
      setTimeout(() => this.alerta = err.error.mensaje, 0);
    });
  }

  editarNoticia(event){
    console.log("este tendria que ser el id" + event.target.attributes.id.nodeValue);
    sessionStorage.setItem('noticiaid', event.target.attributes.id.nodeValue);
    this.router.navigate(['editarNoticia']);
    console.log(event.target.attributes.id.nodeValue);
  }
  eliminar(event) {
    let noticia = this.createUsuarioNoticiaObject(this.userid, event.target.attributes.id.nodeValue);
    this.con.PostRequest('deleteNoticiaUsuario', noticia).toPromise()
      .then(() => {
        this.mostrar();
      })
      .catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

}
