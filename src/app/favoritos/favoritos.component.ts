
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConectionService } from '../conection.service';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.css']
})

export class FavoritosComponent implements OnInit {
  alerta: string = '';
  noticias: any = [];
  userid: string;
  imagen: string = "assets/img/heart.png";

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }

  createUsuarioNoticiaObject(userid: string, noticiaid: string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  mostrar(): void {
    let not = this.createUsuarioObject(this.userid);
    this.con.PostRequest('noticiasLike', not).toPromise()
      .then((res) => {
        console.log(res);
        for (let i in res) {
          this.noticias = res[i];
          console.log(this.noticias);
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  eliminar(event): void{
    let noticia = this.createUsuarioNoticiaObject(this.userid, event.target.attributes.id.nodeValue);
    this.con.PostRequest('deleteNoticiaLike', noticia).toPromise()
      .then(() => {
        this.mostrar();
      })
      .catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }
}
