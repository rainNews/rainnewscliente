import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {
  correo: string = '';
  password: string = '';

  alerta: string = '';

  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
  }

  createLoginObject(correo: string, password: string) {
    return { correo: correo, password: password };
  }

  login(): void {
    let login = this.createLoginObject(this.correo, this.password);
    this.con.PostRequest('inicioUsuario', login).toPromise()
      .then((res) => {
        sessionStorage.setItem('userid', res.userid);
          this.router.navigate(['noticias'])
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

}
