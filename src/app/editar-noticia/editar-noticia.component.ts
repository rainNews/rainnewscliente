import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-noticia',
  templateUrl: './editar-noticia.component.html',
  styleUrls: ['./editar-noticia.component.css']
})
export class EditarNoticiaComponent implements OnInit {
  userid: string;
  noticiaid:string;
  noticia: any = [];
  nuevoEditadorNoticia: string;
  exito: string;
  alerta: string = '';
  color:string = "#65ff00";
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    this.noticiaid = sessionStorage.getItem('noticiaid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    else if(!this.noticiaid){
      this.router.navigate(['misHistorias']);
    }
    this.mostrar();
  }

  createEditarNoticiaObject(noticiaid:string, noticia: string) {
    return { noticiaid: noticiaid, noticia: noticia };
  }
  
  createLeerComentarioObject(noticiaid:string) {
    return { noticiaid: noticiaid };
  }
  
  mostrar(): void {
    console.log("este es el id del comentario"+this.noticiaid);
    let noticia = this.createLeerComentarioObject(this.noticiaid);
    this.con.PostRequest('getNoticia',noticia).toPromise()
    .then((res)=>{
        for(let v in res){
          this.noticia = res[v];
          this.nuevoEditadorNoticia= this.noticia.noticia;
          console.log(this.noticia);
        }
    }).catch((err) => {
      setTimeout(()=> this.alerta = err.console.error.mensaje,0);
    });
  }

  Editar():void{
    console.log("este es el id del comentario"+this.noticiaid);
    let comentario = this.createEditarNoticiaObject(this.noticiaid, this.nuevoEditadorNoticia);
    this.con.PostRequest('postUpdateNoticia',comentario).toPromise()
    .then(()=>{
      this.color = "#65ff00";
      this.exito = "Cambio realizado !!!"
      setTimeout(() => {
        this.router.navigate(['misHistorias']);
       }, 2000);
    }).catch((err)=>{
      setTimeout(()=> this.alerta = err.console.error.mensaje, 0);
    });
  }

}
