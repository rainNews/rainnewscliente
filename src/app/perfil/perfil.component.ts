import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../conection.service';
import { Router } from '@angular/router';
import { Input } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  userid: string;
  alerta: string = '';
  usuario: any = [];
  contra: any = {
    passnueva1: "",
    passnueva2: "",
    passanti3: ""
  };
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createUsuarioObject(userid: string) {
    return { userid: userid };
  }

  createUsuarioEdicionObject(userid: string, apellido: string, correo: string, nombre: string, password: string) {
    return { userid: userid, apellido: apellido, correo: correo, nombre: nombre, password: password };
  }

  mostrar(): void {
    let usuario = this.createUsuarioObject(this.userid)
    this.con.PostRequest('usuario', usuario).toPromise()
      .then((res) => {
        for (let i in res) {
          this.usuario = res[i];
          console.log(this.usuario);
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  /*
  para modificar el usuario
   
  para obtener los datos del usuario:
  * post a "usuario"
  * enviar el parametro "userid"
  * yo te devuelvo un objeto usuario con apellido, correo, nombre y password
   
  para actualizar el usuario:
  * post a "postUsuario"
  * enviar parametro "userid", "apellido", "correo", "nombre", "password"
  * yo te devuelvo un estado 200
   
  */

  submit() {

    this.contra.passnueva1 = (<HTMLInputElement>document.getElementById("pass1")).value
    this.contra.passnueva2 = (<HTMLInputElement>document.getElementById("pass2")).value
    this.contra.passanti3 = (<HTMLInputElement>document.getElementById("pass3")).value
    this.usuario.nombre = (<HTMLInputElement>document.getElementById("nombre")).value
    this.usuario.apellido = (<HTMLInputElement>document.getElementById("apellido")).value
    if (this.contra.passnueva1 == this.contra.passnueva2) {
      if (this.contra.passanti3 == this.usuario.password) {
        this.usuario.password = this.contra.passnueva1;
        console.log(this.contra.passnueva1);
        console.log(this.contra.passnueva2);
        console.log(this.contra.passanti3);
        let usuario = this.createUsuarioEdicionObject(this.userid, this.usuario.apellido, this.usuario.correo, this.usuario.nombre, this.usuario.password);
        this.con.PostRequest('postUsuario', usuario).toPromise()
          .then((res) => {
            console.log(res);
            this.mostrar();
          }).catch((err) => {
            setTimeout(() => this.alerta = err.error.mensaje, 0);
          });
      }
    }
  }
}
