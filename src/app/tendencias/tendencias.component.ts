
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConectionService } from '../conection.service';

@Component({
  selector: 'app-tendencias',
  templateUrl: './tendencias.component.html',
  styleUrls: ['./tendencias.component.css']
})
export class TendenciasComponent implements OnInit {
  alerta: string = '';
  noticias: any = [];
  userid: string;
  imagen: string = "assets/img/heart.png";
  limite: string = "4";
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid) {
      this.router.navigate(['login']);
    }
    this.mostrar();
  }

  createUsuarioObject(limite: string) {
    return { limite: limite };
  }

  createUsuarioNoticiaObject(userid: string, noticiaid: string) {
    return { userid: userid, noticiaid: noticiaid };
  }

  mostrar(): void {
    console.log("entro");
    let not = this.createUsuarioObject(this.limite);
    this.con.PostRequest('getTendencias', not).toPromise()
      .then((res) => {
        console.log(res);
        for (let i in res) {
          this.noticias = res[i];
          console.log(this.noticias);
        }
      }).catch((err) => {
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  mostrarMas(): void {
    var stringToConvert = this.limite;

    if(!isNaN(Number(stringToConvert))){
      var numberValue = Number(stringToConvert)+5;
      this.limite = numberValue.toString();
      this.mostrar();
      console.log(numberValue);
    } else{
        console.log('Not a Number');
    }
  }
}
