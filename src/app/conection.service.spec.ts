import { TestBed } from '@angular/core/testing';

import { ConectionService } from './conection.service';

import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('ConectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ ConectionService ],
    imports: [ HttpClientModule ]
  }));

  it('should be created', () => {
    const service: ConectionService = TestBed.get(ConectionService);
    expect(service).toBeTruthy();
  });
});
